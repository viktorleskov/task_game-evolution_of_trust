package useless;

import com.game.leskov.Choice;
import com.game.leskov.model.Player;
import com.game.leskov.view.View;

import java.util.List;

public class ConsoleView extends View {

    @Override
    public String showMenu() {
        String message = super.showMenu();
        System.out.println(message);
        return message;
    }

    @Override
    public String showRound(int round) {
        String message = super.showRound(round);
        System.out.println(message);
        return message;
    }

    @Override
    public String showAlert(String a) {
        String message = super.showAlert(a);
        System.out.println(message);
        return message;
    }

    @Override
    public String showRoundResult(Player enemy, Choice p, Choice e, int deltaP, int deltaE, int points) {
        String message = super.showRoundResult(enemy, p, e, deltaP, deltaE, points);
        System.out.println(message);
        return message;
    }

    @Override
    public String showGameStart(Player enemy) {
        String message = super.showGameStart(enemy);
        System.out.println(message);
        return message;
    }

    @Override
    public String showGameEnd() {
        String message = super.showGameEnd();
        System.out.println(message);
        return message;
    }

    @Override
    public String showTournamentResults(List<String> names, List<Integer> scores) {
        String message = super.showTournamentResults(names, scores);
        System.out.println(message);
        return message;
    }
}
