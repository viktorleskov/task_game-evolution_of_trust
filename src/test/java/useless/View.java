package com.game.leskov.view;

import com.game.leskov.Choice;
import com.game.leskov.model.Player;

import java.util.List;

public abstract class View {

    public String showMenu() {
        return "Please choose the difficulty";
    }

    public String showRound(int round) {
        return "Round " + round + ".\nChoose what would you like to do: cheat or cooperate (1/2)?\"";
    }

    public String showAlert(String a) {
        return "Alert!!! " + a + "!!!";
    }

    public String showRoundResult(Player enemy, Choice p, Choice e, int deltaP, int deltaE, int points) {
        return String.format("Your choice: %s. Enemy choice: %s. %+d / %+d\nYour total score: %d\n", p, e, deltaP, deltaE, points);
    }

    public String showGameStart(Player enemy) {
        return "New game vs enemy started.";
    }

    public String showGameEnd() {
        return "The game is ended!";
    }

    public String showTournamentResults(List<String> names, List<Integer> scores) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < names.size(); i++)
            result.append(names.get(i)).append(": ").append(scores.get(i)).append("\n");
        return result.toString();
    }
}

