package useless;

import com.game.leskov.Choice;
import com.game.leskov.Difficulty;

import java.util.Scanner;

public class ConsoleReader implements Reader {
    private Scanner in = new Scanner(System.in);

    public Difficulty readDifficulty() {
        String a = in.nextLine().toLowerCase();
        if (a.equals("easy"))
            return Difficulty.EASY;
        if (a.equals("medium"))
            return Difficulty.MEDIUM;
        if (a.equals("pro"))
            return Difficulty.PRO;
        return null;
    }

    public Choice readChoice() {
        int a = in.nextInt();
        if (a == 1)
            return Choice.CHEAT;
        if (a == 2)
            return Choice.COOPERATE;
        return null;
    }

    public String getName() {
        System.out.println("Please enter your name: ");
        return in.nextLine();
    }
}
