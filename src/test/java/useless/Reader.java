package useless;

import com.game.leskov.Choice;
import com.game.leskov.Difficulty;

public interface Reader {
    public Difficulty readDifficulty();
    public Choice readChoice();
    public String getName();
}
