package testandlab;

import org.apache.logging.log4j.*;


public class Log4jTest {
    private static final Logger LOG = LogManager.getLogger(Log4jTest.class);

    public static void main(String[] args) {
        LOG.trace("trace");
        LOG.info("Info");
        LOG.warn("Warn");
        LOG.error("Hi man");
        LOG.fatal("Povnyj tryndec");
    }
}
