package com.game.telegramlogger.util;

/**
 * Util class StringPrepare
 * Created to modify a character string before posting using the post method
 * @author Viktor Leskov
 * @version 1.0
 */
public class StringPrepare {
    private StringPrepare(){}
    /**
     * Method prepareForPost
     * replaces each space in the sentence with a control sequence %20,
     * which is a space character code
     * @return a sentence that is ready to send with the post method
     */
    public static String prepareForPost(String message){
        StringBuilder result = new StringBuilder();
        byte[] messageBytes = message.getBytes();
        for(byte characterCode: messageBytes){
            if(characterCode==' '){
                result.append("%20");
                continue;
            }
            result.append((char)characterCode);
        }
        return result.toString();
    }
}
