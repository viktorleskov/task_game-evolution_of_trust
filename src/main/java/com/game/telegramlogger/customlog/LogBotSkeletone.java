package com.game.telegramlogger.customlog;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import java.io.IOException;
import static com.game.telegramlogger.util.Constants.*;

/**
 * LogBotSkeletone Basic Interface
 * @author Viktor Leskov
 * @version 1.0
 */
public interface LogBotSkeletone {
    /**
     * Default method sendMessage
     * builds request and execute it
     * @param message - message to send
     */
    default void sendMessage(String message){
        HttpClient client = HttpClientBuilder.create().build();
        HttpPost request = new HttpPost(String.format(BASE_URL, DEFAULT_TOKEN,DEFAULT_CHAT_ID,message));
        try {
            client.execute(request);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
