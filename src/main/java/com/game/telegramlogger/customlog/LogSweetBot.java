package com.game.telegramlogger.customlog;

import com.game.telegramlogger.util.StringPrepare;
import org.apache.logging.log4j.Logger;

/**
 * Class LogSweetBot - custom logger with the ability to send messages via telegram
 * @author Viktor Leskov
 * @version 1.0
 */
public class LogSweetBot implements LogBotSkeletone {
    /**Field lOG - composition*/
    private final Logger LOG;
    /**Constructor
     * @param LOG - Logger which will be inside
     */
    public LogSweetBot(Logger LOG){
        this.LOG = LOG;
    }
    /**
     * Method show
     * @return logger for standard logging
     */
    public Logger show(){
        return LOG;
    }
    /**
     * Method sendMessageAndLog
     * sends messages in telegram and logs as usual
     * @param message - message to send
     */
    public void sendMessageAndLog(String message){
        sendMessage(StringPrepare.prepareForPost(message));
        LOG.info(String.format("Message '%s' pushed to your telegram. Check it",message));
    }
}
