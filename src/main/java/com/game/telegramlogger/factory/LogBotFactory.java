package com.game.telegramlogger.factory;

import com.game.telegramlogger.customlog.LogSweetBot;
import org.apache.logging.log4j.LogManager;
/**
 * Class LogBotFactory
 * @author Viktor Leskov
 * @version 1.0
 */
public class LogBotFactory {
    /**
     * Method getBot
     * @return modified logger that can send messages via telegram
     */
    public static LogSweetBot getBot(Class<?> clazz){
        return new LogSweetBot(LogManager.getLogger(clazz));
    }
}
