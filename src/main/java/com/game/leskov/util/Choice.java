package com.game.leskov.util;
/**
 * enum Choice
 * Contains choice list
 * @author Viktor Leskov
 * @version 1.0
 */
public enum Choice {
    CHEAT, COOPERATE
}
