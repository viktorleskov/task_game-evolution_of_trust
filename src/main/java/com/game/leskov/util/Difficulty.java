package com.game.leskov.util;
/**
 * enum Difficulty
 * Contains difficulty list
 * @author Viktor Leskov
 * @version 1.0
 */
public enum Difficulty {
    EASY, MEDIUM, PRO
}