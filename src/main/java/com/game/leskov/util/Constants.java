package com.game.leskov.util;

/**
 * interface Constants
 * Contains constants for view
 * @author Viktor Leskov
 * @version 1.0
 */
public interface Constants {
    //Integer constants for view
    int DEFAULT_VBOX_SPACING = 25;
    int DEFAULT_MENU_WIDTH = 350;
    int DEFAULT_MENU_HEIGHT = 250;

    //Integer constants for controller
    int MIN_ROUNDS_VALUE = 4;
    int APPENDED_ROUNDS_VALUE = 4;

    //Integer constants for difficulty level
    int NUM_OF_BOTS_FOR_EASY_DIFFICULTY = 4;
    int NUM_OF_BOTS_FOR_MEDIUM_DIFFICULTY = 6;
    int NUM_OF_BOTS_FOR_PRO_DIFFICULTY = 8;

    //String constants
    String TITLE = "Evolution of trust" ;
    String ABOUT = "This game is made for EPAM TA lab by Viktor.\nGood game well play!" ;
    String RULES = "The rules of this game:\n" +
            "You have a choice - to cheat or cooperate\n" +
            "Your opponent also has that choice.\n" +
            "After you have made a move, count the number of your points.\n" +
            "If both players cooperated - both get 2 points.\n" +
            "If the first deceived the second - the first gets 3 points and the second loses one point\n" +
            "If both players decide to cheat - no one gets anything.\n" +
            "The number of players varies depending on the difficulty.\n" +
            "All players play against all.\n" +
            "When everyone has finished playing, the sum of points is calculated and the one with the most points wins";
}
