package com.game.leskov.controller;

import com.game.leskov.model.BotFactory;
import com.game.leskov.model.HumanPlayer;
import com.game.leskov.model.Player;
import com.game.leskov.model.bots.*;
import com.game.leskov.util.Choice;
import com.game.leskov.util.Difficulty;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import static com.game.leskov.util.Choice.COOPERATE;
import static com.game.leskov.util.Constants.APPENDED_ROUNDS_VALUE;
import static com.game.leskov.util.Constants.MIN_ROUNDS_VALUE;
import static com.game.leskov.util.Difficulty.EASY;
import static com.game.leskov.util.Difficulty.MEDIUM;
/**
 * Class Server with fields <b>server</b>, <b>deltaF</b>, <b>deltaS</b>
 * @author Viktor Leskov
 * @version 1.0
 */
public class Server {
    /**field server*/
    private static Server server = new Server();
    /**field deltaF*/
    private int deltaF;
    /**field deltaS*/
    private int deltaS;
    /**
     * Method server()
     * @return reference to server field
     */
    public static Server getServer() {
        return server;
    }
    /**
     * Method startTournament(int, Difficulty)
     * @param bots - num of bots
     * @param difficulty - game difficulty
     */
    public void startTournament(int bots, Difficulty difficulty) { //
        new Thread(() -> makeTournament(bots, difficulty)).start(); //new thread running
    }
    /**
     * Method makeTournament(int, Difficulty)
     * @param bots - num of bots
     * @param difficulty - game difficulty
     */
    public void makeTournament(int bots, Difficulty difficulty) {
        List<Player> players = getBots(bots, difficulty); //Bots initialize
        HumanPlayer player = new HumanPlayer();
        players.add(player); //adds player to players list
        int n = players.size();
        for (int i = 0; i < n; i++) {
            for (int j = i + 1; j < n; j++) {
                makeGame(players.get(i), players.get(j), bots); //All players plays against all
            }
        }
        player.processTournamentResults(players);
    }
    /**
     * Method playRound(int, Player, Player, int)
     * round 1 vs 1
     * @param round - round number
     * @param first - first player
     * @param second - second player
     * @param totalOpponents - number of opponents
     */
    private void playRound(int round, Player first, Player second, int totalOpponents) { //one round 1 vs 1
        Choice f = first.makeMove(first.getOpponentNumber(), totalOpponents, first.getPoints(), deltaF, deltaS);
        Choice s = second.makeMove(second.getOpponentNumber(), totalOpponents, second.getPoints(), deltaS, deltaF);
        deltaF = 0; //delta for first Player
        deltaS = 0; //delta for second Player
        if (f == COOPERATE) {
            deltaF--;
            deltaS += 3;
        }
        if (s == COOPERATE) {
            deltaF += 3;
            deltaS--;
        }
        first.processResult(round, second, f, s, deltaF, deltaS);
        second.processResult(round, first, s, f, deltaS, deltaF);
    }
    /**
     * Method makeRound(int, Player, Player, int)
     * 1 vs 1 all rounds(4-8)
     * @param first - first player
     * @param second - second player
     * @param totalOpponents - number of opponents
     */
    private void makeGame(Player first, Player second, int totalOpponents) {
        deltaF = 0;
        deltaS = 0;
        first.startGame(second);
        second.startGame(first);
        Random rand = new Random();
        int rounds = MIN_ROUNDS_VALUE + rand.nextInt(APPENDED_ROUNDS_VALUE);
        for (int i = 1; i <= rounds; i++) {
            playRound(i, first, second, totalOpponents);
        }
        first.endGame();
        second.endGame();
    }
    /**
     * Method getBots(int, Difficulty)
     * @param n - number of bots
     * @param difficulty - for bots difficulty
     * @return bots list
     */
    private List<Player> getBots(int n, Difficulty difficulty) {
        List<BotFactory> constructors; //list for constructors links
        if (difficulty == EASY) {
            constructors = Arrays.asList(CrazyBot::new, AngryBot::new, CheatBot::new, CheatBot::new,
                    CooperateBot::new, CooperateBot::new);
        }
        else if (difficulty == MEDIUM) {
            constructors = Arrays.asList(CrazyBot::new, AngryBot::new, CheatBot::new, CooperateBot::new,
                    CopyCat::new, SherlockBot::new);
        }
        else {
            constructors = Arrays.asList(CrazyBot::new, AngryBot::new, CheatBot::new, CooperateBot::new,
                    CopyCat::new, SherlockBot::new, SherlockBot::new, CopyCat::new);
        }
        List<Player> bots = new ArrayList<>(); //list of bots
        for (int i = 0; i < n; i++) {
            bots.add(randomChoice(constructors).makeBot()); //filling bots list randomly
        }
        return bots;
    }
    /**
     * Method randomChoice(List<T>)
     * @param list - list with elements
     * @return random element from list
     */
    private <T> T randomChoice(List<T> list) { //вибір рандомного конструктора з списку конструкторів.
        Random r = new Random();
        int choice = r.nextInt(list.size());
        return list.get(choice);
    }
}
