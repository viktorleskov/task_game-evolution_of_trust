package com.game.leskov.view;

import com.game.leskov.model.HumanPlayer;
import com.game.telegramlogger.customlog.LogSweetBot;
import com.game.telegramlogger.factory.LogBotFactory;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.util.List;

import static com.game.leskov.controller.Server.getServer;
import static com.game.leskov.util.Choice.CHEAT;
import static com.game.leskov.util.Choice.COOPERATE;
import static com.game.leskov.util.Constants.*;
import static com.game.leskov.util.Difficulty.*;

/**
 * Class Window with fields <b>singleton</b>, <b>player</b>, <b>window</b>
 * This class contains an entry point of JavaFX application
 * The Java FX environment passes an Stage object to the 'start()' method parameter and the program starts
 * @author Viktor Leskov
 * @version 1.0
 */
public class Window extends Application {
    /** Field for Logger */
    private static LogSweetBot log = LogBotFactory.getBot(Window.class);
    /** Field for singleton */
    private static Window singleton;
    /** Field for HumanPlayer */
    private HumanPlayer player;
    /** Field for main stage */
    private Stage window;
    /** Fields for scene fillers */
    private VBox menuBox = new VBox(DEFAULT_VBOX_SPACING);
    private VBox infoBox = new VBox(DEFAULT_VBOX_SPACING);
    private VBox gameBox = new VBox(DEFAULT_VBOX_SPACING);
    private VBox difficultyBox = new VBox (DEFAULT_VBOX_SPACING);
    /** Fields for scenes */
    private Scene menu = new Scene(menuBox, DEFAULT_MENU_WIDTH, DEFAULT_MENU_HEIGHT);
    private Scene info = new Scene(infoBox, DEFAULT_MENU_WIDTH*2, DEFAULT_MENU_HEIGHT);
    private Scene game = new Scene(gameBox, DEFAULT_MENU_WIDTH, DEFAULT_MENU_HEIGHT);
    private Scene difficulty = new Scene(difficultyBox, DEFAULT_MENU_WIDTH, DEFAULT_MENU_HEIGHT);
    /** Fields for primary elements of main menu scene and battle scene */
    private Button newGameButton = new Button("NEW GAME");
    private Button easyButton = new Button("EASY");
    private Button mediumButton = new Button("MEDIUM");
    private Button proButton = new Button("PRO");
    private Button cheatButton = new Button("CHEAT");
    private Button cooperateButton = new Button("COOPERATE");
    private Button rulesButton = new Button("RULES");
    private Button aboutButton = new Button("ABOUT");
    private Button exitButton = new Button("EXIT");
    /** Fields for secondary elements */
    private Button backButton = new Button("BACK");
    private Button backButton2 = new Button("BACK");
    /** Fields for label */
    private Label infoLabel = new Label();
    private Label opponentLabel = new Label();
    private Label totalScoreLabel = new Label();
    private Label scoreLabel = new Label();
    /**
     * Main method
     * Calls the default method for JavaFx application
     * @param args
     */
    public static void main(String[] args) {
        launch(args);
    }
    /**
     * Entry point
     * This method builds a GUI.
     * @param primaryStage - parameter passed by JavaFx environment
     */
    @Override
    public void start(Stage primaryStage) {
        singleton = this;
        //use primaryStage as main
        window = primaryStage;
        window.setTitle(TITLE);
        //set interactions for main menu
        newGameButton.setOnAction(e -> showDifficulty());
        exitButton.setOnAction(e -> System.exit(0));
        aboutButton.setOnAction(e -> showInfo(ABOUT));
        rulesButton.setOnAction(e -> showInfo(RULES));
        //set interactions for other menu
        backButton.setOnAction(e -> showMenu());
        backButton2.setOnAction(e -> showMenu());
        easyButton.setOnAction(e -> getServer().startTournament(NUM_OF_BOTS_FOR_EASY_DIFFICULTY, EASY));
        mediumButton.setOnAction(e -> getServer().startTournament(NUM_OF_BOTS_FOR_MEDIUM_DIFFICULTY, MEDIUM));
        proButton.setOnAction(e -> getServer().startTournament(NUM_OF_BOTS_FOR_PRO_DIFFICULTY, PRO));
        cheatButton.setOnAction(e -> player.choose(CHEAT));
        cooperateButton.setOnAction(e -> player.choose(COOPERATE));
        //filling scenes
        menuBox.getChildren().addAll(newGameButton, rulesButton, aboutButton, exitButton);
        infoBox.getChildren().addAll(infoLabel, backButton);
        gameBox.getChildren().addAll(opponentLabel, totalScoreLabel, scoreLabel, cheatButton, cooperateButton);
        difficultyBox.getChildren().addAll(easyButton, mediumButton, proButton, backButton2);
        //set main menu as window scene and show
        showMenu();
        window.show();
    }
    /**
     * Method getWindow
     * @return copy of reference to singleton
     */
    public static Window getWindow() {
        return singleton;
    }
    /**
     * Method showMenu
     * Sets the current scene to the menu
     */
    public void showMenu() {
        window.setScene(menu);
    }
    /**
     * Method showDifficulty
     * Sets the current scene to the difficulty
     */
    public void showDifficulty() {
        window.setScene(difficulty);
    }
    /**
     * Method showInfo
     * Shows the text that is specified through the parameters
     * and sets the current scene to the info.
     * @param text - the text to be displayed.
     */
    public void showInfo(String text) {
        infoLabel.setText(text);
        window.setScene(info);
        window.show();
    }
    /**
     * Method showGame
     * Sets the current scene to the Game
     * @param opponent - current opponent number
     * @param totalOpponents - num of opponents
     * @param totalScore - player total Score
     * @param playerScore - player score for last round
     * @param enemyScore - enemy score for last round
     */
    public void showGame(int opponent, int totalOpponents, int totalScore, int playerScore, int enemyScore) {
        opponentLabel.setText(String.format("Opponent: %d of %d", opponent, totalOpponents));
        totalScoreLabel.setText(String.format("Your total score: %d", totalScore));
        scoreLabel.setText(String.format("| %d | - | %d |", playerScore, enemyScore));
        window.setScene(game);
    }
    /**
     * Method setPlayer
     * sets player
     * @param p - player reference
     */
    public void setPlayer(HumanPlayer p) {
        player = p;
    }
    /**
     * Method showResults
     * Displays the end result of the game
     * @param names - list of players names
     * @param scores - list of players scores
     */
    public void showResults(List<String> names, List<Integer> scores){
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < names.size(); i++)
            result.append(names.get(i)).append(": ").append(scores.get(i)).append("\n");
        infoLabel.setText(result.toString());
        window.setScene(info);
    }
}
