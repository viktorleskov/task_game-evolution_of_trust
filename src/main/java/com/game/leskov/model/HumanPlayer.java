package com.game.leskov.model;

import com.game.leskov.util.Choice;
import com.game.telegramlogger.customlog.LogSweetBot;
import com.game.telegramlogger.factory.LogBotFactory;
import javafx.application.Platform;

import java.util.List;
import java.util.stream.Collectors;

import static com.game.leskov.view.Window.getWindow;
/**
 * Class HumanPlayer with fields <b>choice</b>
 * @author Viktor Leskov
 * @version 1.0
 */
public class HumanPlayer extends Player {
    private static LogSweetBot log = LogBotFactory.getBot(HumanPlayer.class);
    /**field Choice*/
    private Choice choice;
    /**default Constructor calls parent constructor*/
    public HumanPlayer() {
        super("Player");
    }
    /**
     * Method makeMove(int, int, int, int, int)
     * wait for Players choice
     * @see #choose(Choice)
     * @param totalOpponents - number of opponents
     * @param opponent - num of opponent
     * @param totalScore - total player score
     * @param playerScore - player score this round
     * @param enemyScore - enemy score this round
     * @return player Choice
     */
    @Override
    public synchronized Choice makeMove(int opponent, int totalOpponents, int totalScore, int playerScore, int enemyScore){
        super.makeMove(opponent, totalOpponents, totalScore, playerScore, enemyScore); //round++
        //user interface update
        Platform.runLater(() -> getWindow().showGame(opponent, totalOpponents, totalScore, playerScore, enemyScore));
        choice = null;
        while (choice == null) {
            try {
                log.sendMessageAndLog("Cheat or cooperate? That is the question.");
                wait(); //waiting for notify in choose(Choice) method
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return choice;
    }
    /**
     * Method choose(choice)
     * method called by view
     * notify makeMove() after choice change
     * @param choice - new choice by player
     */
    public synchronized void choose(Choice choice) {
        this.choice = choice;
        notify();  //notify makeMove()
    }
    /**
     * processResult
     * calls parent method processResult(...)
     */
    @Override
    public void processResult(int round, Player enemy, Choice playerChoice, Choice enemyChoice, int deltaP, int deltaE){
        super.processResult(round, enemy, playerChoice, enemyChoice, deltaP, deltaE);
    }
    /**
     * startGame(Player) method
     * initialize game
     * calls view method setPlayer and
     * set up this HumanPlayer as player in view(Window) class
     */
    @Override
    public void startGame(Player enemy) {
        super.startGame(enemy); //opponent number++, round=0
        getWindow().setPlayer(this);
    }
    @Override
    public void endGame() {
    }
    /**
     * processTournamentResults(List) method
     * show players and scores via view
     * @param players - list with players after game
     */
    public void processTournamentResults(List<Player> players) {
        //update ui with new info
        Platform.runLater(() -> getWindow().showResults(players.stream().map(Player::getName).collect(Collectors.toList()),
                players.stream().map(Player::getPoints).collect(Collectors.toList())));
    }
}
