package com.game.leskov.model;

import com.game.leskov.util.Choice;
/**
 * Class Player with fields <b>name</b>, <b>points</b>, <b>opponentNumber</b>, <b>round</b>
 * @author Viktor Leskov
 * @version 1.0
 */
public abstract class Player {
    /**field name*/
    private final String name;
    /**field points*/
    private int points;
    /**field opponentNumber*/
    private int opponentNumber;
    /**field round*/
    private int round;
    /**Protected constructor for Player
     * @param name - for private field name
     */
    protected Player(String name) {
        this.name = name;
    }
    /**
     * Method startGame(Player)
     * increments opponentNumber
     */
    public void startGame(Player enemy) {
        opponentNumber++;
        round = 0;
    }
    /**
     * Method makeMove(int, int, int, int, int)
     * increments round
     */
    public Choice makeMove(int opponent, int totalOpponents, int totalScore, int playerScore, int enemyScore){
        round++;
        return null;
    }
    /**
     * Method processResult(int, Player, Choice, Choice, int, int)
     * increments total points
     */
    public void processResult(int round, Player enemy, Choice playerChoice, Choice enemyChoice, int deltaP, int deltaE){
        points += deltaP;
    }
    public abstract void endGame();
    /**
     * Method getPoints()
     * getter method for points field
     * @return points
     */
    public int getPoints() {
        return points;
    }
    /**
     * Method getOpponentNumber()
     * getter method for opponentNumber field
     * @return opponentNumber
     */
    public int getOpponentNumber() {
        return opponentNumber;
    }
    /**
     * Method getName
     * getter method for name field
     * @return name
     */
    public String getName() {
        return name;
    }
    /**
     * Method getRound
     * getter method for round field
     * @return round
     */
    protected int getRound(){ //чого протектед?
        return round;
    }
}
