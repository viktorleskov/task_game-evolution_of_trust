package com.game.leskov.model;

import com.game.leskov.model.bots.Bot;
/**
 * functional Interface BotFactory
 * for bots production
 * @author Viktor Leskov
 * @version 1.0
 */
@FunctionalInterface
public interface BotFactory {
    Bot makeBot();
}
