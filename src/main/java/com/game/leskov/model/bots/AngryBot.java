package com.game.leskov.model.bots;

import com.game.leskov.util.Choice;
import com.game.leskov.model.Player;

import static com.game.leskov.util.Choice.CHEAT;
import static com.game.leskov.util.Choice.COOPERATE;

/**
 * Class AngryBot
 * This bot's strategy is to cooperate until the enemy tries to cheat
 * @author Viktor Leskov
 * @version 1.0
 */
public class AngryBot extends Bot {
    private boolean hasCheated;

    public AngryBot() {
        super("AngryBot");
    }
    @Override
    public void startGame(Player enemy) {
        super.startGame(enemy);
        hasCheated = false;
    }
    @Override
    public Choice makeMove(int opponent, int totalOpponents, int totalScore, int playerScore, int enemyScore) {
        super.makeMove(opponent, totalOpponents, totalScore, playerScore, enemyScore);
        if (hasCheated) {
            return CHEAT;
        }
        else{
            return COOPERATE;
        }
    }
    @Override
    public void processResult(int round, Player enemy, Choice playerChoice, Choice enemyChoice, int deltaP, int deltaE){
        super.processResult(round, enemy, playerChoice, enemyChoice, deltaP, deltaE);
        if (enemyChoice == CHEAT) {
            hasCheated = true;
        }
    }
}
