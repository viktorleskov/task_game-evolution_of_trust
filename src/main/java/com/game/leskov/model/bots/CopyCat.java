package com.game.leskov.model.bots;

import com.game.leskov.util.Choice;
import com.game.leskov.model.Player;

import static com.game.leskov.util.Choice.COOPERATE;
/**
 * Class CheatBot
 * This bot starts with cooperate and copying the action of his opponent
 * @author Viktor Leskov
 * @version 1.0
 */
public class CopyCat extends Bot {
    private Choice enemyChoice;

    public CopyCat() {
        super("CopyCat");
    }
    @Override
    public void startGame(Player enemy) {
        super.startGame(enemy);
        enemyChoice = COOPERATE;
    }
    @Override
    public Choice makeMove(int opponent, int totalOpponents, int totalScore, int playerScore, int enemyScore) {
        super.makeMove(opponent, totalOpponents, totalScore, playerScore, enemyScore);
        return enemyChoice;
    }
    @Override
    public void processResult(int round, Player enemy, Choice playerChoice, Choice enemyChoice, int deltaP, int deltaE){
        super.processResult(round, enemy, playerChoice, enemyChoice, deltaP, deltaE);
        this.enemyChoice = enemyChoice;
    }

}
