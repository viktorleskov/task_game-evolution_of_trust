package com.game.leskov.model.bots;

import com.game.leskov.util.Choice;

import static com.game.leskov.util.Choice.COOPERATE;
/**
 * Class CheatBot
 * This bot's strategy is always cooperate
 * @author Viktor Leskov
 * @version 1.0
 */
public class CooperateBot extends Bot {
    public CooperateBot() {
        super("KindBot");
    }
    @Override
    public Choice makeMove(int opponent, int totalOpponents, int totalScore, int playerScore, int enemyScore) {
        super.makeMove(opponent, totalOpponents, totalScore, playerScore, enemyScore);
        return COOPERATE;
    }
}
