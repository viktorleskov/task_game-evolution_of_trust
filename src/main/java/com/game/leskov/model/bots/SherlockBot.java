package com.game.leskov.model.bots;

import com.game.leskov.util.Choice;
import com.game.leskov.model.Player;

import static com.game.leskov.util.Choice.CHEAT;
import static com.game.leskov.util.Choice.COOPERATE;

public class SherlockBot extends Bot {
    private boolean hasCheated = false;
    private Choice enemyChoice;

    public SherlockBot() {
        super("Sherlock");
    }
    @Override
    public void startGame(Player enemy) {
        super.startGame(enemy);
        hasCheated = false;
    }
    @Override
    public Choice makeMove(int opponent, int totalOpponents, int totalScore, int playerScore, int enemyScore) {
        super.makeMove(opponent, totalOpponents, totalScore, playerScore, enemyScore);
        int round = getRound();
        if (round == 1 || round == 3 || round == 4) {
            return COOPERATE;
        }
        else if (round == 2) {
            return CHEAT;
        }
        else if (hasCheated) {
            return enemyChoice;
        }
        else{
            return CHEAT;
        }
    }
    @Override
    public void processResult(int round, Player enemy, Choice playerChoice, Choice enemyChoice, int deltaP, int deltaE){
        super.processResult(round, enemy, playerChoice, enemyChoice, deltaP, deltaE);
        this.enemyChoice = enemyChoice;
        if (round < 5 && this.enemyChoice == CHEAT) {
            hasCheated = true;
        }
    }
}
