package com.game.leskov.model.bots;

import com.game.leskov.util.Choice;

import java.util.Random;
/**
 * Class CrazyBot
 * random choice everytime
 */
public class CrazyBot extends Bot {
    public CrazyBot() {
        super("CrazyBot");
    }
    @Override
    public Choice makeMove(int opponent, int totalOpponents, int totalScore, int playerScore, int enemyScore) {
        super.makeMove(opponent, totalOpponents, totalScore, playerScore, enemyScore);
        Random rand = new Random();
        if (rand.nextBoolean()) {
            return Choice.CHEAT;
        }
        else {
            return Choice.COOPERATE;
        }
    }
}

