package com.game.leskov.model.bots;

import com.game.leskov.model.Player;
/**
 * Class Bot
 * Base class for other bots
 * @author Viktor Leskov
 * @version 1.0
 */
public abstract class Bot extends Player {
    protected Bot(String name) {
        super(name);
    }
    @Override
    public void startGame(Player enemy) {
    }
    @Override
    public void endGame() {
    }
}