package com.game.leskov.model.bots;

import com.game.leskov.util.Choice;
import com.game.telegramlogger.customlog.LogSweetBot;
import com.game.telegramlogger.factory.LogBotFactory;

import static com.game.leskov.util.Choice.CHEAT;
/**
 * Class CheatBot
 * This bot's strategy is always cheating
 * @author Viktor Leskov
 * @version 1.0
 */
public class CheatBot extends Bot {
    private static LogSweetBot log = LogBotFactory.getBot(CheatBot.class);
    public CheatBot() {
        super("Cheater");
    }
    @Override
    public Choice makeMove(int opponent, int totalOpponents, int totalScore, int playerScore, int enemyScore) {
        super.makeMove(opponent, totalOpponents, totalScore, playerScore, enemyScore);
        return CHEAT;
    }
}
